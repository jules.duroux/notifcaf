﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using HtmlAgilityPack;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Notif.Models;

namespace Notif.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IEmailSender _emailSender;
        private readonly NotifContext _context;

        public ValuesController(IEmailSender emailSender, NotifContext notifContext)
        {
            _emailSender = emailSender;
            _context = notifContext;
        }

        // GET api/values
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> GetAsync()
        {

            string baseurl = "https://www.cafchambery.com";
            var web = new HtmlWeb();
            int annee = 2019;
            List<int> listeMois = new List<int> { 2, 3, 4, 5 };
            List<string> emails = new List<string> { "mylane.barnier@gadz.org", "maxime.lescure@gadz.org" };

            List<Sortie> DejaEnvoyees = _context.Sortie.ToList();

            StringBuilder page = new StringBuilder();
            foreach (var mois in listeMois)
            {
                var doc = web.Load(string.Format("https://www.cafchambery.com/agenda/ski-alpinisme.html?month={0}&year={1}", mois,annee));

                HtmlNode table = doc.GetElementbyId("agenda");
                var elements = table.SelectNodes("//table/tr/td/a");
                string jour = "0";
                foreach (var element in elements)
                {
                    //Récupérer le jour
                    var idAttribute = element.Attributes.Where(a => a.Name == "id").FirstOrDefault();
                    if (idAttribute != null && idAttribute.Value.Contains("day"))
                    {
                        jour = element.Attributes.Where(a => a.Name == "id").FirstOrDefault().Value.Replace("day", "");
                        string html = element.InnerHtml;
                    }

                    //Récupérer le reste
                    var classAttribute = element.Attributes.Where(a => a.Name == "class").FirstOrDefault();
                    if (classAttribute != null && classAttribute.Value == "agenda-evt-debut")
                    {
                        string url = element.Attributes.Where(a => a.Name == "href").FirstOrDefault().Value;


                        
                        DateTime dateEvenement = DateTime.Parse(string.Format("{0}/{1}/{2}", jour, mois, annee));
                        //Si la date est un weekend :
                        var journee = dateEvenement.DayOfWeek;

                        if (journee == DayOfWeek.Saturday || journee == DayOfWeek.Sunday)
                        {
                            //Vérifier que l'élément n'existe pas déjà dans la base de donnéee. S'il n'existe pas, on ajoute le corps dans les éléments à envoyer et on l'ajoute en base
                            if (!DejaEnvoyees.Where(s => s.uri == url).Any())
                            {
                                string html = element.InnerHtml;

                                page.AppendFormat("<h1>le {0}/{1}/{2} :</h1>", jour, mois, annee);
                                page.AppendFormat("<a href=\"{0}{1}\">Cliquez sur le lien pour accéder au site </a>", baseurl, url);
                                page.Append("<h1>description :</h1>");
                                page.Append(html);

                                //On ajoute à la base de données pour ne pas le réenvoyer plus tard
                                Sortie nouvelleEntree = new Sortie();
                                nouvelleEntree.uri = url;
                                _context.Sortie.Add(nouvelleEntree);
                            }
                        }
                    }
                    

                }
            }


            if (page.Length > 0)
            {
                _context.SaveChanges();
                //On envoie le mail
                foreach (string email in emails)
                {
                    await _emailSender.SendEmailAsync(
                    email,
                    "Nouveauté CAF de Chamberry",
                    page.ToString());
                }
            }

            return new string[] { };
        }

    }
}
